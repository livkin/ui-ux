![map](issues_screens/01.png)
---------

[CodePen](https://codepen.io/bdrb00/full/jOmbXdj)
======================
1. [x] "Home" - pointer on hower
2. [x] "Exp.." - pointer, change bg on hover ("Imp.." like)
3. [x] "Imp.." - pointer
4. [x] "Add .." - change bg jn hover
    - [x] Similar buttons style
    - [x] Exp.../Imp... less important 
6. [x] Group visually label and its field by ":", color, border or stmth else
7. [x] Margins between
8.  [x] Paddings, center
9.  [x] Margin right, text wrap
10. [x] Should be no line transfer in the date line
11. [x] If its possible, should not be line transfer in the user name line
    - [x] new line for user name
12. [ ] No space below date/user panel
13. [x] More between cards
14. [ ] No space here
15. [ ] Different fonts, sizes, colors for different semantics
    - [x] user
    - [x] breadcrumbs
    - [x] filters
    - [x] text on cards
16. [x] The same width for cards
17. [x] Margins, font, color, center for "(c)"
18. [x] No space below footer
19. [x] Photo/pict for current user :) Pointer if it is a menu.
20. [x] The same border radius for all elements
